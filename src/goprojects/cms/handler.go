package cms

import (
	"net/http"
	"time"
	"strings"
)

func ServeIndex(w http.ResponseWriter, r *http.Request) {
	p := &Page{
		Title:   "Go Projects CMS",
		Content: "Welcome to our home page!",
		Posts: []*Post{
			&Post{
				Title:         "Hello, world!",
				Content:       "Hello world",
				DatePublished: time.Now(),
			},
			&Post{
				Title:         "A Post with Comments",
				Content:       "Here's a controversial post. It's sure to attract comments.",
				DatePublished: time.Now().Add(time.Hour),
				Comments: []*Comment{
					&Comment{
						Author:        "Edu Finn",
						Comment:       "Nevermind, I quess I just commented on my own post...",
						DatePublished: time.Now().Add(time.Hour / 2),
					},
				},
			},
		},
	}

	Templ.ExecuteTemplate(w, "page", p)
}

func ServePage(w http.ResponseWriter, r *http.Request) {
	path := strings.TrimLeft(r.URL.Path, "/page/")

	if path == "" {
		pages, err := GetPages()
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		Templ.ExecuteTemplate(w, "pages", pages)
	}

	p, err := GetPage(path)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	Templ.ExecuteTemplate(w, "page", p)
}

func ServePost(w http.ResponseWriter, r *http.Request) {
	path := strings.TrimLeft(r.URL.Path, "/post/")

	if path == "" {
		http.NotFound(w, r)
		return
	}

	p := &Post{
		Title:   strings.ToTitle(path),
		Content: "Here is my post",
		Comments: []*Comment{
			&Comment{
				Author:        "Edu Finn",
				Comment:       "Looks great!",
				DatePublished: time.Now(),
			},
		},
	}

	Templ.ExecuteTemplate(w, "post", p)
}

// HandleNew handles preview logic
func HandleNew(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":
		Templ.ExecuteTemplate(w, "new", nil)

	case "POST":
		title := r.FormValue("title")
		contnet := r.FormValue("content")
		contnetType := r.FormValue("content-type")
		r.ParseForm()

		if contnetType == "page" {
			p := &Page{
				Title:   title,
				Content: contnet,
			}
			_, err := CreatePage(p)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			Templ.ExecuteTemplate(w, "page", p)
			return
		}

		if contnetType == "post" {
			Templ.ExecuteTemplate(w, "post", &Post{
				Title:   title,
				Content: contnet,
			})
			return
		}
	default:
		http.Error(w, "Method not supported: "+r.Method, http.StatusMethodNotAllowed)
	}
}
